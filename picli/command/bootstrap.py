import click
from enum import Enum
from typing import Union

from picli import logger
from picli.bootstrap_runner import BaseBootstrap
from picli.command.util import exclusive

LOG = logger.get_logger(__name__)


class SupportedBootstrapLanguages(Enum):
    Python = "python"
    Default = "None"


@click.command()
@click.option("--project-name", "-p", help="Project name for generated project")
@click.option(
    "--template-url", "-t", default=None, help="CookeCutter template repository to use"
)
@click.option(
    "--language",
    "-l",
    type=click.Choice([str(lang.value) for lang in SupportedBootstrapLanguages]),
    help=f"Language template to pull",
)
@click.option("--branch", "-b", default="master", help="Checkout branch")
@click.option("--config", "-c", help="Path to the template configuration file")
def bootstrap(
    project_name: str,
    template_url: str,
    language: Union[str, SupportedBootstrapLanguages],
    config: str,
    branch: str,
):
    if not exclusive(click.get_current_context().params, ["template_url", "language"]):
        raise click.UsageError("Option template-url may not be specified with language")
    if not (template_url or language):
        raise click.UsageError("Either template-url or language must be specified")
    runner = BaseBootstrap()
    runner.bootstrap(
        project=project_name,
        template_url=template_url,
        language=language,
        checkout_branch=branch,
        config=config,
    )
