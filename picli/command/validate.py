import click
from picli import logger
from picli.config import BaseConfig

LOG = logger.get_logger(__name__)


@click.command()
@click.option("--stages", help="Comma separated list of stages to run")
@click.pass_context
def validate(context, stages):
    """
    Run a validation of the stage configuration against a remote endpoint
    :param context:
    :param stages:
    :return:
    """
    debug = context.obj.get("args")["debug"]
    config_directory = context.obj.get("args")["config"]
    config = BaseConfig(config_directory, debug=debug)
    if stages:
        stages_list = stages.split(",")
        sequence = config.get_sequence(stages=stages_list)
    else:
        stages_list = [s.name for s in config.stages]
        sequence = config.get_sequence(stages=stages_list)

    config.validate(sequence)
