import json
import logging
import os
import tempfile
from urllib.parse import urlparse
import zipfile

import requests
from picli import logger, util
from picli.model import stage_schema
from piperci.gman.exceptions import TaskError
from piperci.storeman.exceptions import StoremanError
from piperci.sri import generate_sri, hash_to_urlsafeb64

LOG = logger.get_logger(__name__)


class Stage:
    def __init__(self, stage_def, base_config):
        self.name = stage_def.get("name")
        self.stage_config = base_config
        self.dependencies = []
        self.tasks = stage_def.get("tasks")
        self.config = stage_def.get("config")
        self._validate_stage_config(stage_def)
        if self.stage_config.debug:
            LOG.setLevel(logging.DEBUG)

    def _check_thread_status(self, thread_id=None, stage=None):
        """
        Waits for task_id to be "status". Defaults to completed
        :param thread_id: task_id string
        :param stage: The stage that the thread is tied to
        :return:
        """
        if stage is None:
            stage = self.name
        LOG.debug(f"Checking if thread_id {thread_id} has completed")
        try:
            self.stage_config.gman_client.wait_for_thread(
                thread_id=thread_id, retry_max=60
            )
            self.stage_config.update_state({stage: {"state": "complete"}})
            return True
        except TimeoutError:
            message = f"Timeout detected waiting for thread_id {thread_id} to complete."
            util.sysexit_with_message(message)
        except TaskError:
            self.stage_config.update_state({self.name: {"state": "failed"}})
            event_failures = self.stage_config.gman_client.get_events(
                thread_id=thread_id,
                query_filter=lambda x: x.get("status") == "failed",
            )
            message = (
                f"Remote job for stage {self.name} did not complete successfully."
                f"\n\n{util.safe_dump(event_failures)}"
            )
            util.sysexit_with_message(message)

    def _create_project_artifact(self):
        """
        Creates a zipfile of the project and uploads to the configured storage url
        :return: List of artifact dicts returned by util.upload_artifacts
        """
        with tempfile.TemporaryDirectory() as tempdir:
            project_artifact_file = self._zip_project(tempdir).filename
            project_artifact_name = os.path.basename(project_artifact_file)
            project_artifact_hash = generate_sri(project_artifact_file)
            sri_urlsafe = hash_to_urlsafeb64(project_artifact_hash)
            artifact_state = {self.name: {"artifacts": {project_artifact_name: {}}}}
            if self.stage_config.artman_client.check_artifact_exists(
                sri_urlsafe=sri_urlsafe
            ):
                LOG.debug(f"Artifact exists in ArtMan. Getting artifact data...")
                artifact = self.stage_config.artman_client.get_artifact(
                    gman_client=self.stage_config.gman_client,
                    sri_urlsafe=sri_urlsafe
                )
                LOG.debug(f"Artifact: {artifact}")
                try:
                    artifact_state[self.name]["artifacts"][project_artifact_name] = {
                        "artifact_uri": artifact[0]["uri"],
                        "artifact_sri": str(project_artifact_hash),
                        "state": "found",
                    }

                    post_artifact_data = {
                        "task_id": self.stage_config.state[self.name]["client_task_id"],
                        "uri": artifact[0]["uri"],
                        "type": "source",
                        "sri": str(project_artifact_hash),
                    }
                except (KeyError, IndexError) as e:
                    util.sysexit_with_message(f"Error formatting artifact data. \n{e}")
                self.stage_config.artman_client.create_artifact(**post_artifact_data)
                self.stage_config.update_state(artifact_state)
            else:
                LOG.debug(
                    f"Artifact not found. Uploading to bucket {self.stage_config.run_id}"
                )
                try:
                    self.stage_config.storage_client.upload_file(
                        self.stage_config.run_id,
                        f"artifacts/{project_artifact_name}",
                        project_artifact_file,
                    )
                except StoremanError as e:
                    LOG.debug(
                        f"Encountered error while contacting the storage.\n" f"{e}"
                    )
                    util.sysexit_with_message(
                        f"Encountered error while contacting the storage.\n" f"{e}"
                    )
                artifact_uri = (
                    f"minio://{self.stage_config.storage['hostname']}/"
                    f"{self.stage_config.run_id}/artifacts/"
                    f"{project_artifact_name}"
                )
                try:
                    artifact_state[self.name]["artifacts"][project_artifact_name] = {
                        "artifact_uri": artifact_uri,
                        "artifact_sri": str(project_artifact_hash),
                        "state": "uploaded",
                    }
                    post_artifact_data = {
                        "task_id": self.stage_config.state[self.name]["client_task_id"],
                        "uri": artifact_uri,
                        "type": "source",
                        "sri": str(project_artifact_hash),
                    }
                except (KeyError, IndexError) as e:
                    util.sysexit_with_message(
                        util.sysexit_with_message(
                            f"Error formatting artifact data.\n{e}"
                        )
                    )
                self.stage_config.artman_client.create_artifact(**post_artifact_data)
                self.stage_config.update_state(artifact_state),

    def _get_artifacts(self, artifact_types):
        """
        Returns a list of artifacts from ArtMan for a given run_id and
        artifact type.
        :param run_id: The runID to search for
        """
        self._wait_on_local_state()

        thread_id_for_stage = self.stage_config.state.get(self.name).get("thread_id")
        artifacts = {}
        for artifact_type in artifact_types:
            targets = self.stage_config.artman_client.get_artifact(
                thread_id=thread_id_for_stage,
                gman_client=self.stage_config.gman_client,
                query_filter=lambda x: x.get("type") == artifact_type
            )
            artifacts.update({artifact_type: targets})

        return artifacts

    def _is_dependent_stage_state_completed(self):
        stages_not_complete = [
            stage
            for stage in self.dependencies
            if self.stage_config.state.get(stage.name)
            and self.stage_config.state.get(stage.name).get("state") != "completed"
        ]
        errors = []
        for stage in stages_not_complete:
            LOG.info(f"Checking if stage {stage.name} has completed")
            task_id_from_state = self.stage_config.state.get(stage.name).get(
                "thread_id"
            )
            if not self._check_thread_status(task_id_from_state, stage=stage.name):
                errors.append(stage.name)
        if len(errors):
            return False
        else:
            return True

    def _submit_job(self, resource_url, task_id, config=None, validate=False):
        """
        Sends a PiperCI job to a remote endpoint
        :param resource_url: The URL of the endpoint
        :param task_id: task_id
        :param config: Configs to pass to the FaaS
        :return: JSON response from the endpoint
        """
        headers = {"Content-Type": "application/json"}
        task_data = {
            "run_id": self.stage_config.run_id,
            "project": self.stage_config.project_name,
            "parent_id": task_id,
            "config": config,
            "stage": self.name,
            "task_manager_uri": self.stage_config.gman_client.url
        }
        if validate:
            task_data.update({"only_validate": True})
        else:
            artifacts = [
                artifact["artifact_sri"]
                for _, artifact in
                self.stage_config.state[self.name]["artifacts"].items()
            ]
            task_data.update({"artifacts": artifacts})
        try:
            LOG.debug(f"Sending task data: {task_data}")
            r = requests.post(resource_url, data=json.dumps(task_data), headers=headers)
            r.raise_for_status()
        except requests.exceptions.HTTPError as e:
            message = (
                f"Received bad status code from {resource_url}. \n\n{r.text}\n\n{e}"
            )
            if validate:
                self.stage_config.update_state({self.name: {"validation": "failed"}})
            else:
                self.stage_config.update_state({self.name: {"state": "failed"}})
            self.stage_config.gman_client.update_task(
                task_id=task_id,
                status="failed",
                message=f"{message}"
            )
            util.sysexit_with_message(message)
        except requests.exceptions.RequestException as e:
            message = f"Failed to call {resource_url} gateway. \n\n{e}"
            if validate:
                self.stage_config.update_state({self.name: {"validation": "failed"}})
            else:
                self.stage_config.update_state({self.name: {"state": "failed"}})
            self.stage_config.gman_client.update_task(
                task_id=task_id,
                status="failed",
                message=message
            )
            util.sysexit_with_message(message)

        if validate:
            self.stage_config.update_state({self.name: {"validation": "success"}})
        else:
            self.stage_config.update_state(
                {
                    self.name: {
                        "state": "running",
                        "client_task_id": task_id,
                        "thread_id": r.json().get("task").get("thread_id"),
                    }
                }
            )
        self.stage_config.gman_client.update_task(
            task_id=task_id,
            status="completed",
            message="Client received acceptance of job. Completing client task."
        )

        return r

    def _validate_stage_config(self, stage_def):
        """
        Validate the loaded configuration object.
        Validations are defined in model/base_schema.py
        :return: None. Exit if errors are found.
        """
        errors = stage_schema.validate(stage_def)
        if errors:
            msg = (
                f"Failed to validate stage definition."
                f"Stage definition:\n"
                f"{util.safe_dump(stage_def)}\n"
                f"{errors}"
            )
            util.sysexit_with_message(msg)
        pass

    def _wait_on_local_state(self):
        """
        Wait for
        :return:
        """
        try:
            thread_id_from_state = self.stage_config.state[self.name]["thread_id"]
        except KeyError as e:
            message = (f"No threadID or stage for stage {self.name} found in state file.",
                       f"Missing: {e}.\n Aborting")
            util.sysexit_with_message(message)
        try:
            self._check_thread_status(thread_id=thread_id_from_state)
            LOG.debug(f"Thread has completed!")
        except TaskError:
            message = f"Waiting for job completion timed out"
            util.sysexit_with_message(message)

    def _zip_project(self, destination):
        """
        Zips all files in the project and returns the zipfile
        object.
        :param destination: Path to create the zipfile in
        :return: ZipFile
        """
        zip_file = zipfile.ZipFile(
            f"{destination}/{self.stage_config.project_name}.zip",
            "w",
            zipfile.ZIP_DEFLATED,
        )

        for root, dirs, files in os.walk(self.stage_config.base_path):
            for file in files:

                state_path = os.path.realpath(
                    os.path.join(self.stage_config.base_path, "piperci.d/default/state")
                )
                if os.path.commonpath(
                    [os.path.abspath(state_path)]
                ) == os.path.commonpath(
                    [
                        os.path.abspath(state_path),
                        os.path.abspath(os.path.join(root, file)),
                    ]
                ):
                    continue
                else:
                    zip_file.write(
                        os.path.join(root, file),
                        os.path.relpath(
                            os.path.join(root, file), self.stage_config.base_path
                        ),
                    )

        zip_file.close()

        return zip_file

    def add_dependency(self, stage):
        """
        Add a stage object as a dependency to this stage.
        :param stage: Stage object
        :return:
        """
        self.dependencies.append(stage)

    def download(self, file_type="all"):
        """
        Downloads artifacts and logs generated during a run.
        :param run_id: The run_id to display
        :return: None
        """
        try:
            thread_id_for_stage = self.stage_config.state[self.name]["thread_id"]
        except KeyError as e:
            util.sysexit_with_message(f"Could not find threadID or stage in state file.\n"
                                      f"The key that was missing was: {e}")

        LOG.info(f"Downloading {file_type} for thread_id: {thread_id_for_stage}")
        download_types = (
            [file_type]
            if file_type != "all"
            else self.stage_config.artman_client.artifact_types()
        )
        download_artifacts = self._get_artifacts(artifact_types=download_types)

        for artifact_type, artifacts in download_artifacts.items():
            if not len(artifacts):
                LOG.warn(f"No {artifact_type} artifacts found for stage {self.name}")
            for artifact in artifacts:
                LOG.debug(f"Found {artifact_type} {artifact['uri']}")
                local_path = self.gen_local_path(
                    os.path.basename(urlparse(artifact["uri"]).path)
                )
                LOG.debug(
                    f"Downloading {artifact_type} {artifact['uri']} to {local_path}"
                )
                self.stage_config.storage_client.download_file(
                    artifact["uri"], local_path
                )

    def display(self, log_type="stdout"):
        """
        Displays the output of the stage by downloading the log artifact into a
        temporary directory.
        :param run_id: The run_id to display
        :return: None
        """
        log_types = (
            [log_type]
            if log_type != "all"
            else self.stage_config.artman_client.artifact_types()
        )
        log_artifacts = self._get_artifacts(artifact_types=log_types)
        for artifact_type, artifacts in log_artifacts.items():
            if not len(artifacts):
                LOG.warn(f"No {artifact_type} artifacts found for stage {self.name}")
            for artifact in artifacts:
                LOG.debug(f"Found {artifact_type} {artifact['uri']}")
                local_path = self.gen_local_path(
                    os.path.basename(urlparse(artifact["uri"]).path)
                )
                if os.path.exists(local_path):
                    with open(local_path) as f:
                        LOG.warn(f.read())
                else:
                    self.download(file_type=artifact_type)
                    with open(local_path) as f:
                        LOG.warn(f.read())

    def execute(self, wait=False):
        """
        Execute a stage.
        First we check if all of our dependent stages are complete remotely.
        Then we check if the stage we are running is already marked as complete
        in the local state file.
        Then, request a new taskID from GMan for client execution tracking.
        For every glob in the stage config we parse out which resource to invoke.
        Calls the requested resource and waits for the returned taskID to finish executing
        :param wait: Boolean. Whether to wait for results or not
        :return: None
        """
        if (
            self.stage_config.state.get(self.name)
            and self.stage_config.state.get(self.name).get("state") == "completed"
        ):
            LOG.info(
                f"Stage {self.name} marked complete in local state file. Skipping..."
            )
            return
        if not self._is_dependent_stage_state_completed():
            message = (
                f"Stage dependencies '{[dep.name for dep in self.dependencies]}' "
                f"are not complete. Check your state file"
            )
            util.sysexit_with_message(message)

        try:
            task = self.stage_config.gman_client.new_task(
                run_id=self.stage_config.run_id,
                project=self.stage_config.project_name,
                caller="picli",
                status="started"
            )
        except requests.RequestException as e:
            message = "Failed to request taskID from GMan. Aborting"
            LOG.debug(f"{message}\n{e}")
            util.sysexit_with_message(message)
        LOG.debug(f"Received task {task} from gman")

        self.stage_config.update_state(
            {self.name: {"state": "started", "client_task_id": task["task"]["task_id"]}}
        )

        self._create_project_artifact()

        if not self.tasks:
            util.sysexit_with_message("No configuration found for tasks")

        for task_config in self.tasks:
            task_url = self.stage_config.faas_endpoint + task_config.get("uri")
            task_config = task_config.get("config")
            LOG.info(f"Sending to {task_url}")

            job_results = self._submit_job(
                task_url, task.get("task").get("task_id"), config=task_config
            )
            LOG.debug(
                f"Job submitted for stage {self.name} to {task_url} "
                f"with task_id {job_results.json()['task']['task_id']}"
            )
            if wait:
                LOG.info("waiting")
                self.display()

    def gen_local_path(self, path):
        """
        Generates a local path for use when downloading files.

        :param path: Name of the file to generate a local path for.
        :return: Generated local path.
        """

        return f"{self.stage_config.state_directory}/{self.name}-{path}"

    def validate(self):
        """
        Validates a stage's configuration data against a remote endpoint.
        :return: Boolean
        """

        LOG.info("Starting validation")
        try:
            task = self.stage_config.gman_client.new_task(
                run_id=self.stage_config.run_id,
                project=self.stage_config.project_name,
                caller="picli",
                status="started"
            )
        except requests.RequestException as e:
            message = "Failed to request taskID from GMan. Aborting"
            LOG.debug(f"{message}\n{e}")
            util.sysexit_with_message(message)

        LOG.debug(f"Received task {task} from gman")

        self.stage_config.update_state(
            {self.name: {"state": "started", "client_task_id": task["task"]["task_id"]}}
        )

        if not self.tasks:
            util.sysexit_with_message("No configuration found for tasks")

        for task_config in self.tasks:
            task_url = self.stage_config.faas_endpoint + task_config.get("uri")
            task_config = task_config.get("config")
            LOG.info(f"Sending to {task_url}")
            job_results = self._submit_job(
                task_url, task["task"]["task_id"], config=task_config, validate=True
            )

            if job_results.status_code == 200:
                LOG.info("Validation succeeded.")
            else:
                LOG.info("Validation failed.")
