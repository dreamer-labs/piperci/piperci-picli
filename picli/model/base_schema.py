from marshmallow import Schema, fields


class StorageSchema(Schema):
    type = fields.Str(required=True)
    hostname = fields.Str(required=True)
    access_key = fields.Str(required=True)
    secret_key = fields.Str(required=True)


class ConfigSchema(Schema):
    project_name = fields.Str(required=True)
    version = fields.Str(required=True)
    gman_url = fields.Str(required=True)
    faas_endpoint = fields.Str(required=True)
    storage = fields.Nested(StorageSchema)


def validate(config):
    schema = ConfigSchema()
    return schema.validate(config)
