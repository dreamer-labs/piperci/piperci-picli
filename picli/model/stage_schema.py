from marshmallow import Schema, fields


class ResourceSchema(Schema):
    name = fields.Str(required=True)
    uri = fields.Str(Required=True)
    config = fields.Dict(unknown=True)


class StageSchema(Schema):
    name = fields.Str(required=True)
    deps = fields.List(fields.Str(), required=True, allow_none=True)
    tasks = fields.List(fields.Nested(ResourceSchema))


def validate(config):
    schema = StageSchema()
    return schema.validate(config)
