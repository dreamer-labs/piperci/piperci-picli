Developer's Guide
=================

This section containers information for developers who are
working on PiperCI. 

If you are just getting started with PiperCI please read these
documents. If you find any information that is missing a Pull Request
is more than welcome.

If you want to set up a development environment please read the
:ref:`Testing  Guide<testing>`

.. toctree::
   :maxdepth: 1

   architecture
   api
   datamodel
   testing
   contributing
