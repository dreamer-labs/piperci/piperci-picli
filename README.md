# PiperCI Command Line - PiCli

[![Build Status](https://gitlab.com/dreamer-labs/piperci/piperci-picli/badges/master/pipeline.svg)](https://gitlab.com/dreamer-labs/piperci/piperci-picli)

PiCli is a python client for PiperCI, a CI-pipeline validation framework.

## Getting Started

Refer to the [PiperCI installer](https://piperci.dreamer-labs.net/piperci-installer/README) for installation and setup.

### Prerequisites

* Python 3.7
* (Optionally) virtualenv
* PiperCI OpenFaaS functions installed. (See Getting Started)

### Installing

#### Docker Installation

```

tox -e build-docker

```

#### Python installation

```

python setup.py install

```

## Using

To run PiCli:

### Execute a lint

```

picli run --stages=style --clean --wait

```

### Execute a validation

```

picli run --stages=validate --clean --wait

```

### CLI Arguments

#### debug

```

picli --debug run

```

Debug information will be displayed

#### run

```

picli run

```

The main entrypoint to PiCli. This will execute the stages defined in your stages.yml file
by sending HTTP requests to the resources you have provided.

#### display

```

picli display

```

Display job results from the commandline. This will read your local state file for the taskID of each stage,
query GMan for job status, and then download artifacts for each job.

## Running the tests

To run the lint tests use tox: `tox -e lint`

To run the unit tests use tox: `tox -e unittest`

## Contributing

Please read [Contributing Guide](https://piperci.dreamer-labs.net/project-info/contributing) for details on our code of conduct, and the process for submitting pull requests.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/dreamer-labs/piperci/piperci-picli/-/tags).

## Authors

See also the list of [contributors](https://gitlab.com/dreamer-labs/piperci/piperci-picli/-/graphs/master) who participated in this project.

## License

[LICENSE](https://gitlab.com/dreamer-labs/piperci/piperci-picli/blob/master/LICENSE)

## Acknowledgments

* Inspiration for the CLI framework came from the Ansible Molecule project
