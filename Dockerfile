# This is a multi-stage build which requires Docker 17.05 or higher
FROM python:3.7-alpine as builder

WORKDIR /usr/src/picli

ENV PACKAGES="\
	git \
    "
RUN apk add --update --no-cache ${PACKAGES}


ADD .git .git
ADD picli picli
ADD setup.py setup.py
ADD requirements.txt requirements.txt
RUN \
    pip wheel \
    -w dist .

# ✄---------------------------------------------------------------------
# This is an actual target container:

FROM python:3.7-alpine
LABEL maintainer "DREAM <dream@globalinfotek.com>"

ENV PIP_INSTALL_ARGS="\
    --only-binary :all: \
    --no-index \
    -f /usr/src/picli/dist \
    "

RUN apk add --update --no-cache git

COPY --from=builder \
    /usr/src/picli/dist \
    /usr/src/picli/dist

RUN \
    pip install ${PIP_INSTALL_ARGS} "piperci-picli" && \
    apk del --no-cache ${BUILD_DEPS} && \
    rm -rf /root/.cache

RUN mkdir -p /app

WORKDIR /app
CMD ["picli", "run", "--clean"]
