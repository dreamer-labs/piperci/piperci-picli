import mock
import pytest

from click.testing import CliRunner
from picli.bootstrap_runner import BaseBootstrap
from picli.shell import main


@pytest.mark.parametrize(
    "unsupported_language", ["go", "rust", "javascript", "c++", "cpp", "c"]
)
def test_bootstrap_rejects_unsupported_languages(unsupported_language):
    with mock.patch.object(BaseBootstrap, "bootstrap", autospec=True) as mock_bootstrap:
        runner = CliRunner()
        command = pytest.helpers.build_bootstrap_command(
            l=unsupported_language, p="test"
        )

        result = runner.invoke(main, command, catch_exceptions=False)
        assert result.exit_code == 2


def test_bootstrap_uses_template_url():
    with mock.patch.object(BaseBootstrap, "bootstrap") as mock_bootstrap:
        runner = CliRunner()
        command = pytest.helpers.build_bootstrap_command(
            p="test", t="http://test_url.git"
        )
        result = runner.invoke(main, command, catch_exceptions=False)
        args, kwargs = mock_bootstrap.call_args

        assert result.exit_code == 0
        assert "template_url" in kwargs.keys()
        assert "http://test_url.git" in kwargs.values()


def test_bootstrap_fails_exclusive_arguments():
    runner = CliRunner()
    command = pytest.helpers.build_bootstrap_command(t="blah", l="python")
    result = runner.invoke(main, command)

    assert isinstance(result.exception, SystemExit)

def test_bootstrap_fails_required_parameters():
    runner = CliRunner()
    command = pytest.helpers.build_bootstrap_command()
    result = runner.invoke(main, command)

    assert isinstance(result.exception, SystemExit)
