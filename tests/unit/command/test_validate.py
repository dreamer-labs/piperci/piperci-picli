import mock
import pytest
from click.testing import CliRunner
from picli.config import BaseConfig
from picli.shell import main


@pytest.mark.parametrize(
    "input_stages, expected_stages",
    [
        ("dependent,default", ["default", "dependent"]),
        ("default,dependent", ["default", "dependent"]),
        ("", ["default", "dependent"]),
    ],
)
def test_run_validates_given_sequence(input_stages, expected_stages, mocker):
    with mock.patch.object(BaseConfig, "validate", spec=BaseConfig) as mock_validate:
        runner = CliRunner()
        with runner.isolated_filesystem():
            pytest.helpers.write_piperci_files(
                pytest.helpers.piperci_directories(),
                pytest.helpers.piperci_config_fixture,
                pytest.helpers.piperci_stages_fixture,
            )
            command = (
                ["validate", "--stages", input_stages] if len(input_stages) else ["validate"]
            )
            runner.invoke(main, command, catch_exceptions=False)
            mock_validate.assert_called_once_with(expected_stages)


def test_run_validates_without_execute(mocker):
    with mock.patch.object(BaseConfig, "validate", spec=BaseConfig) as mock_validate:
        mock_execute = mocker.patch.object(BaseConfig, "execute", spec=BaseConfig)
        runner = CliRunner()
        with runner.isolated_filesystem():
            pytest.helpers.write_piperci_files(
                pytest.helpers.piperci_directories(),
                pytest.helpers.piperci_config_fixture,
                pytest.helpers.piperci_stages_fixture,
            )
            command = (
                ["validate"]
            )
            runner.invoke(main, command, catch_exceptions=False)
            mock_execute.assert_not_called()
            mock_validate.assert_called_once()


@pytest.mark.parametrize("input_stages, expected_failure", [("no_stage", 1)])
def test_run_fails_invalid_sequence(input_stages, expected_failure):
    with mock.patch.object(BaseConfig, "validate", spec=BaseConfig):
        runner = CliRunner()
        with runner.isolated_filesystem():
            pytest.helpers.write_piperci_files(
                pytest.helpers.piperci_directories(),
                pytest.helpers.piperci_config_fixture,
                pytest.helpers.piperci_stages_fixture,
            )
            command = (
                ["validate", "--stages", input_stages] if len(input_stages) else ["validate"]
            )
            results = runner.invoke(main, command)
            assert results.exit_code == expected_failure
