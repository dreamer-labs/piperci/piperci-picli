import os
import pytest
from click.testing import CliRunner
from picli.shell import main


# As currently invoked, this has a network connectivity dependence for this test
@pytest.mark.parametrize(
    "input_project_name, expected_project_name, input_language, template_url, cfg, branch",
    [
        (None, "blank-project-name", "python", None, None, "master"),
        ("cli-proj-name-passed", "cli-proj-name-passed", "python", None, "", "master"),
        (None, "config-driven-name", "python", None, "config.yml", "master"),
        (
            "override-name",
            "override-name",
            None,
            "https://gitlab.com/dreamer-labs/piperci/piperci-cookiecutter-python",
            "config.yml",
            "master",
        ),
    ],
)
def test_bootstrap_creates_fresh_project(
    input_project_name,
    expected_project_name,
    input_language,
    template_url,
    cfg,
    branch,
    dummy_config_data,
):
    runner = CliRunner()
    command = pytest.helpers.build_bootstrap_command(
        p=input_project_name, l=input_language, t=template_url, c=cfg, b=branch
    )

    with runner.isolated_filesystem():
        if cfg:
            with open(cfg, "w", encoding="utf-8") as f:
                f.write(dummy_config_data)

        result = runner.invoke(main, command, catch_exceptions=False)
        assert result.exit_code == 0

        assert os.path.isdir(f"./{expected_project_name}")
        assert os.path.isdir(f"./{expected_project_name}/piperci.d")
        assert os.path.isdir(f"./{expected_project_name}/piperci.d/default")
        assert os.path.isfile(f"./{expected_project_name}/.gitlab-ci.yml")
